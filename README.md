## This repository described an installation of OpenStack Rocky (Ceph is used as storage backend) using openstack-ansible script

## Lab Logical Architecture 
The lab items are one chassis containing four blades server(bdf-blade01, bdf-blade02, bdf-blade05, bdf-blade06) 
and one storage disk partionned to be used by bdf-blade02, bdf-blade05, bdf-blade06


![alt text](/lab/lab-logical-architecture.PNG?raw=true)
##
## 
In this lab, we are using ceph as backend for OpenStack storage components  
Ceph is deployed in the four blades, bdf-blade01 is the ceph-mon, bdf-blade02, bdf-blade05, bdf-blade06 are the ceph-osd. 

![alt text](/lab/cluster-architecture.PNG?raw=true)
##
##

# Here we have only one load balancer installed in bdf-blade01
# Ideally the load balancer should not use the Infrastructure hosts.
# Dedicated hardware is best for improved performance and security.

## Disk available in the blade
    bdf-blade01 (ceph-mon)
       - /dev/sda
       
    bdf-blade02 (ceph-osd)
       - /dev/sda
       - /dev/sdb
       
    bdf-blade05 (ceph-osd)
       - /dev/sda
       - /dev/sdb
       
    bdf-blade06 (ceph-osd)
       - /dev/sda
       - /dev/sdb
Disk **/dev/sdb** is mapped to the storage disk partition [2, 5, 6] repectively for bdf-blade02, bdf-blade05, bdf-blade06.    
/dev/sdb is used by ceph while /dev/sda is used by the blade's system disk. 
# Prepare Blade's disk for Ceph cluster.  
Be sure your /dev/sdb got no important data first! Below command will erase everything
     
     umount /dev/sdb
     parted -s /dev/sdb mklabel gpt mkpart primary xfs 0% 100%
     mkfs.xfs /dev/sdb -f
     
## Configure the deployment Host (the host from where ansible will be launched)
Update package source lists, upgrade the system packages and kernel and install additional software packages:  

     sudo apt-get update   
     sudo apt-get dist-upgrade    
     sudo reboot   
     sudo apt-get install aptitude build-essential git ntp ntpdate openssh-server python-dev sudo   

* Option 1: Use the actual preconfigure files   

Untar openstack-ansible.tar to /opt/openstack-ansible    
Change to the /opt/openstack-ansible directory, and run the Ansible bootstrap script   
    
    scripts/bootstrap-ansible.sh

* Option 2: Use official git  

Clone the latest stable release of the OpenStack-Ansible Git repository in the /opt/openstack-ansible directory  

    git clone -b 18.1.2 https://git.openstack.org/openstack/openstack-ansible /opt/openstack-ansible
	
Change to the /opt/openstack-ansible   

    scripts/bootstrap-ansible.sh

Configure SSH keys, Ansible uses SSH to connect the deployment host and target hosts.   
In our case bdf-blade01 is used as deployment host   

    sudo su ##(go root)
    ssh-keygen    
    ssh-copy-id bdf-blade01
    ssh-copy-id bdf-blade02
    ssh-copy-id bdf-blade05
    ssh-copy-id bdf-blade06
    
## Prepare the target hosts
For each blade (bdf-blade01, bdf-blade02, bdf-blade05, bdf-blade06)
Update package source lists, upgrade the system packages and kernel and install additional software packages:  
    
    apt-get update
    apt-get dist-upgrade  
    apt-get install bridge-utils debootstrap ifenslave ifenslave-2.6 lsof lvm2 chrony openssh-server sudo tcpdump vlan python
    apt install linux-image-extra-$(uname -r)
    
Add the appropriate kernel modules to the /etc/modules file to enable VLAN and bond interfaces:  
  
    echo 'bonding' >> /etc/modules
    echo '8021q' >> /etc/modules
    service chrony restart

## Blades Network Information
Each blade has four physical interfaces (eth1, eth2, eth3 and eth4) from which we create four logical interfaces br-vlan, br-vxlan, br-storage, br-mgmt
## 
    #bdf-blade01:
        br-vlan
	    br-vxlan   172.29.240.1
	    br-storage 172.29.244.1
	    br-mgmt -> 10.8.254.101
    #bdf-blade02:
	    br-vlan
	    br-vxlan   172.29.240.2
	    br-storage 172.29.244.2
	    br-mgmt -> 10.8.254.102
    #bdf-blade05:
	   br-vlan
	   br-vxlan   172.29.240.5
	   br-storage 172.29.244.5
	   br-mgmt -> 10.8.254.105	
    #bdf-blade06:
	   br-vlan
	   br-vxlan   172.29.240.6
	   br-storage 172.29.244.6
	   br-mgmt -> 10.8.254.106
	    
## Following is /etc/network/interfaces of bdf-blade01. Change the ip accordingly for the others blades
    #The loopback network interface:
    auto lo
    iface lo inet loopback

    #The primary network interface
    auto eth0
    iface eth0 inet static
	
    #Container/Host management bridge
    auto br-mgmt
    iface br-mgmt inet static
          bridge_stp off
          bridge_waitport 0
          bridge_fd 0
          bridge_ports eth0
          address 10.8.254.101
          netmask 255.255.255.0
          gateway 10.8.254.1
          dns-nameservers 10.8.34.10 10.8.34.14 8.8.8.8 8.8.4.4

    #Internal 1G network
    auto eth1
    iface eth1 inet static

    #OpenStack Networking VLAN bridge
    auto br-vlan
    iface br-vlan inet static
          bridge_stp off
          bridge_waitport 0
          bridge_fd 0
          bridge_ports eth1
          address 172.29.248.1
          netmask 255.255.255.0

    #Storage Network
    auto eth2
    iface eth2 inet manual

    #OpenStack Storage bridge
    auto br-storage
    iface br-storage inet static
          bridge_stp off
          bridge_waitport 0
          bridge_fd 0
          bridge_ports eth2
          address 172.29.244.1
          netmask 255.255.255.0
          mtu 9000	

    #Tunnel VXLAN Network
    auto eth3
    iface eth3 inet static
	
    auto br-vxlan
    iface br-vxlan inet static
         bridge_stp off
         bridge_waitport 0
         bridge_fd 0
         bridge_ports eth3
         address 172.29.240.1
         netmask 255.255.255.0
         mtu 9000
---

## Openstack Ansible Configuration  
*Option 1: Use the actual git file  
Untar openstack_deploy to /etc/openstack_deploy

*Option 2: Use the official rocky git
Copy the contents of the /opt/openstack-ansible/etc/openstack_deploy directory to the /etc/openstack_deploy directory.  
Change to the /etc/openstack_deploy directory.  
Copy the openstack_user_config.yml.example file to /etc/openstack_deploy/openstack_user_config.yml.  
Review the **openstack_user_config.yml** file and make changes to the deployment of your OpenStack environment. (Use the file provided as exemple)
in *ceph-osd_hosts* block we specify the disk (dev/sdb) to used for ceph cluster deployment

    ---
    cidr_networks: &cidr_networks
      container: 10.8.254.0/24
      tunnel: 172.29.240.0/22
      storage: 172.29.244.0/22

    used_ips:
      - "10.8.254.1,10.8.254.110"
      - "172.29.240.1,172.29.240.50"
      - "172.29.244.1,172.29.244.50"
      - "172.29.248.1,172.29.248.50"

    global_overrides:
      cidr_networks: *cidr_networks
      internal_lb_vip_address: 10.8.254.101
      #
      # The below domain name must resolve to an IP address
      # in the CIDR specified in haproxy_keepalived_external_vip_cidr.
      # If using different protocols (https/http) for the public/internal
      # endpoints the two addresses must be different.
      #
      external_lb_vip_address: bdf-blade01.exfo.loc
      management_bridge: "br-mgmt"
      provider_networks:
        - network:
            container_bridge: "br-mgmt"
            container_type: "veth"
            container_interface: "eth1"
            ip_from_q: "container"
            type: "raw"
            group_binds:
              - all_containers
              - hosts
            is_container_address: true
        - network:
            container_bridge: "br-vxlan"
            container_type: "veth"
            container_interface: "eth10"
            ip_from_q: "tunnel"
            type: "vxlan"
            range: "1:1000"
            net_name: "vxlan"
            group_binds:
              - neutron_linuxbridge_agent
        - network:
            container_bridge: "br-vlan"
            container_type: "veth"
            container_interface: "eth12"
            host_bind_override: "br-vlan"
            type: "flat"
            net_name: "flat"
            group_binds:
              - neutron_linuxbridge_agent
        - network:
            container_bridge: "br-vlan"
            container_type: "veth"
            container_interface: "eth11"
            type: "vlan"
            range: "101:200,301:400"
            net_name: "vlan"
            group_binds:
              - neutron_linuxbridge_agent
        - network:
            container_bridge: "br-storage"
            container_type: "veth"
            container_interface: "eth2"
            ip_from_q: "storage"
            type: "raw"
            group_binds:
              - glance_api
              - cinder_api
              - cinder_volume
              - nova_compute
              - ceph-osd

    ###
    ### Infrastructure
    ###

    _infrastructure_hosts: &infrastructure_hosts
      bdf-blade01:
        ip: 10.8.254.101

    # nova hypervisors
    compute_hosts: &compute_hosts
      bdf-blade01:
        ip: 10.8.254.101
      bdf-blade02:
        ip: 10.8.254.102
      bdf-blade05:
        ip: 10.8.254.105
      bdf-blade06:
        ip: 10.8.254.106

    ceph-osd_hosts:
      bdf-blade02:
        ip: 10.8.254.102
        container_vars:
          data:
            - /dev/sdb
          raw_journal:
            - /dev/sdb
      bdf-blade05:
        ip: 10.8.254.105
        container_vars:     
          data:
            - /dev/sdb
          raw_journal:       
            - /dev/sdb
      bdf-blade06:
        ip: 10.8.254.106
        container_vars:   
          data:     
            - /dev/sdb   
          raw_journal:     
            - /dev/sdb

    # galera, memcache, rabbitmq, utility
    shared-infra_hosts: *infrastructure_hosts

    # ceph-mon containers
    ceph-mon_hosts: *infrastructure_hosts

    # repository (apt cache, python packages, etc)
    repo-infra_hosts: *infrastructure_hosts

    # load balancer
    # Ideally the load balancer should not use the Infrastructure hosts.
    # Dedicated hardware is best for improved performance and security.
    haproxy_hosts: *infrastructure_hosts

    # rsyslog server
    log_hosts:
      bdf-blade01:
        ip: 10.8.254.101

    ###
    ### OpenStack
    ###

    # keystone
    identity_hosts: *infrastructure_hosts

    # cinder api services
    storage-infra_hosts: *infrastructure_hosts

    # cinder volume hosts (Ceph RBD-backed)
    storage_hosts: *infrastructure_hosts

    # glance
    image_hosts: *infrastructure_hosts

    # nova api, conductor, etc services
    compute-infra_hosts: *infrastructure_hosts

    # heat
    orchestration_hosts: *infrastructure_hosts

    # horizon
    dashboard_hosts: *infrastructure_hosts

    # neutron server, agents (L3, etc)
    network_hosts: *infrastructure_hosts

    # ceilometer (telemetry data collection)
    metering-infra_hosts: *infrastructure_hosts

    # aodh (telemetry alarm service)
    metering-alarm_hosts: *infrastructure_hosts

    # gnocchi (telemetry metrics storage)
    metrics_hosts: *infrastructure_hosts

    # ceilometer compute agent (telemetry data collection)
    metering-compute_hosts: *compute_hosts

    
    
Review the **user_variables.yml** file to configure global and role specific deployment options. The file contains some example variables and comments but you can get the full list of variables in each role�s specific documentation
**Environment customizations**
The optionally deployed files in /etc/openstack_deploy/env.d allow the customization of Ansible groups.   
This allows the deployer to set whether the services will run in a container (the default), or on the host (on metal).

For this ceph environment, we will run the cinder-volume in a container. To do this you will need to create a **/etc/openstack_deploy/env.d/cinder.yml** containing:

    ---
    # This file contains an example to show how to set
    # the cinder-volume service to run in a container.
    #
    # Important note:
    # When using LVM or any iSCSI-based cinder backends, such as NetApp with
    # iSCSI protocol, the cinder-volume service *must* run on metal.
    # Reference: https://bugs.launchpad.net/ubuntu/+source/lxc/+bug/1226855

    container_skel:
      cinder_volumes_container:
        properties:
          is_metal: false
          
Edit **/etc/openstack_deploy/user_variables.yml** following, *cinder_backends* block is the most important !:
    
    ---
    # Because we have three haproxy nodes, we need
    # to one active LB IP, and we use keepalived for that.
    ## Load Balancer Configuration (haproxy/keepalived)
    haproxy_keepalived_external_vip_cidr: "1.2.3.4/32"
    haproxy_keepalived_internal_vip_cidr: "172.29.236.0/22"
    haproxy_keepalived_external_interface: ens2
    haproxy_keepalived_internal_interface: br-mgmt

    ## Ceph cluster fsid (must be generated before first run)
    ## Generate a uuid using: python -c 'import uuid; print(str(uuid.uuid4()))'
    generate_fsid: false
    fsid: 116f14c4-7fe1-40e4-94eb-9240b63de5c1 # Replace with your generated UUID

    ## ceph-ansible settings
    ## See https://github.com/ceph/ceph-ansible/tree/master/group_vars for
    ## additional configuration options availble.
    monitor_address_block: "{{ cidr_networks.container }}"
    public_network: "{{ cidr_networks.container }}"
    cluster_network: "{{ cidr_networks.storage }}"
    osd_scenario: collocated
    journal_size: 10240 # size in MB
    # ceph-ansible automatically creates pools & keys for OpenStack services
    openstack_config: true
    cinder_ceph_client: cinder
    glance_ceph_client: glance
    glance_default_store: rbd
    glance_rbd_store_pool: images
    nova_libvirt_images_rbd_pool: vms

    cinder_backends:
      RBD:
        volume_driver: cinder.volume.drivers.rbd.RBDDriver
        rbd_pool: volumes
        rbd_ceph_conf: /etc/ceph/ceph.conf
        rbd_store_chunk_size: 8
        volume_backend_name: rbddriver
        rbd_user: "{{ cinder_ceph_client }}"
        rbd_secret_uuid: "{{ cinder_ceph_client_uuid }}"
        report_discard_supported: true

# Configuring service credentials
Configure credentials for each service in the /etc/openstack_deploy/user_secrets.yml  
The keystone_auth_admin_password option configures the admin tenant password for both the OpenStack API and Dashboard access.   

    cd /opt/openstack-ansible
    ./scripts/pw-token-gen.py --file /etc/openstack_deploy/user_secrets.yml
    
## Run Playbooks
The installation process requires running three main playbooks:  
* The setup-hosts.yml Ansible foundation playbook prepares the target hosts for infrastructure and OpenStack services, builds and restarts containers on target hosts, and installs common components into containers on target hosts.   
* The setup-infrastructure.yml Ansible infrastructure playbook installs infrastructure services: Memcached, the repository server, Galera, RabbitMQ, and rsyslog.  
* The setup-openstack.yml OpenStack playbook installs OpenStack services, including Identity (keystone), Image (glance), Block Storage (cinder), Compute (nova), Networking (neutron), etc.  
# Checking the integrity of the configuration files  
Ensure that all the files edited in the /etc/openstack_deploy directory are Ansible YAML compliant.  
Change to the /opt/openstack-ansible/playbooks directory, and run  

    openstack-ansible setup-infrastructure.yml --syntax-check
    
# Run the playbooks to install OpenStack  
Change to the /opt/openstack-ansible/playbooks directory  
Run the host setup playbook  

    openstack-ansible setup-hosts.yml  ###Confirm satisfactory completion with zero items unreachable or failed:  
Run the infrastructure setup playbook:

    openstack-ansible setup-infrastructure.yml   ###Confirm satisfactory completion with zero items unreachable or failed
Run the following command to verify the database cluster:

    ansible galera_container -m shell -a "mysql -h localhost -e 'show status like \"%wsrep_cluster_%\";'"
Run the OpenStack setup playbook

    openstack-ansible setup-openstack.yml   ###Confirm satisfactory completion with zero items unreachable or failed
    
## Verifying OpenStack operation
**Verify the API **  
The utility container provides a CLI environment for additional configuration and testing  
Determine the name of the utility container: 

    lxc-ls | grep utility
    infra1_utility_container-161a4084
Access the utility container:  
   
    lxc-attach -n infra1_utility_container-161a4084
List your openstack users:
   
    . openrc
    openstack user list --os-cloud=default

**Verifying the Dashboard (horizon)**  
With a web browser, access the Dashboard by using the external load balancer IP address defined by the external_lb_vip_address option in the /etc/openstack_deploy/openstack_user_config.yml file. The Dashboard uses HTTPS on port 443.
Authenticate by using the admin user name and the password defined by the keystone_auth_admin_password option in the /etc/openstack_deploy/user_secrets.yml file.  

**Configure Neutron on the blade to handle flat networking**   
Edit **dhcp_agent.ini**, **l3_agent.ini**, **linuxbridge_agent.ini**, **ml2_conf.ini** as per the current /etc/neutron files  

**Create router, physical_network and private network**

    neutron router-create core-router
    neutron net-create private-net --shared
    neutron subnet-create private-net 11.0.0.0/24 --name private-subnet
    neutron router-interface-add core-router private-subnet
    neutron net-create external-net --router:external --provider:physical_network flat --provider:network_type flat

    neutron subnet-create --name external-subnet --allocation-pool start=10.8.254.10,end=10.8.254.20 --disable-dhcp --gateway 10.8.254.1 external-net 10.8.254.0/24
    neutron router-gateway-set core-router external-net

**add route for virtual**

    ip route add 11.0.0.0/24 via virtual_router_ip (virtual router ip, ex: 10.8.254.254)

**Create cirros image**  
Download image online  

    wget http://download.cirros-cloud.net/0.3.4/cirros-0.3.4-x86_64-disk.img
Create glance cirros

    openstack image create --public --disk-format qcow2 --container-format bare --file cirros-0.3.4-x86_64-disk.img cirros
    
##How to create custom image  
In this section we want to customize an centos image to enable ssh login without key.perm on root user  

**Install guestfish package**  

    yum -y install libguestfs-tools

**Generate password hash**  

    openssl passwd -1 yourPassword
    $1$QiSwNHrs$mQG1 ----> output sample
**Download cnetos image**  

    wget https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1708.qcow2
**Enter guestfich console**  

    guestfish -a CentOS-6-x86_64-GenericCloud.qcow2
    run
    list-filesystems
    mount /dev/sda1 /
    vi /etc/shadow
**Edit /etc/shadow**  
    
    Copy the output of the openssl command above and appropriately place it into the /etc/shadow file you have opened with vi, (replace !! in root line)
    Ex: ====  root:!!:17876:0:99999:7 ----become --->   root:$1$QiSwNHrs$mQG1:17876:0:99999:7
**Edit    /etc/cloud/cloud.cfg**  
    
    vi /etc/cloud/cloud.cfg
set disable_root: 0, ssh_pwauth: 1, default_user name: root, lock_passwd: false, passwd:$1$QiSwNHrs$mQG1  
#Example:

    system_info:
      default_user:
        name: root
        lock_passwd: false
        passwd: $1$QiSwNHrs$mQG1   <----------------- password generated previously by openssl
        gecos: Cloud User
        groups: [wheel, adm, systemd-journal]
        sudo: ["ALL=(ALL) NOPASSWD:ALL"]
        shell: /bin/bash
      distro: rhel
      paths:
        cloud_dir: /var/lib/cloud
        templates_dir: /etc/cloud/templates
      ssh_svcname: sshd
    
 **Quit console**
 
    quit
**Create glance image**
    
    . openrc
    openstack image create --public --disk-format qcow2 --container-format bare --file CentOS-6-x86_64-GenericCloud.qcow2 centos7
    